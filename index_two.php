<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
	  <title>TAU</title>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/style.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="assets/libs/conuntdown.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('#clock').countdown('2017/10/30 00:00:00', function (event) {
              /*$(this).html(event.strftime('%D dias %H:%M:%S'));*/
                $(this).html(event.strftime('%H:%M:%S'));
            });
        });
    </script>
  </head>
  <body>

    <div class="painel-full">
      <div class="tipography">

        <span>TAU</span>
        <p class="slogan">Construções Personalizadas</p>

        <div class="panel panel-default painelclock" data-toggle="tooltip" data-placement="top" title="Comming soon">
            <div class="panel-body">
              Comming soon
                <div class="lead" id="clock"></div>
            </div>
        </div>

      </div>
      <!--<img src="assets/imagens/taulogo.png" class="taulogo" />-->
    </div>

    <footer class="contacts">
      <ul>
        <li>
          <i class="icomoon-mail5"></i> <a href="mailto:contato@tau.com.br">contato@tau.com.br</a>
        </li>
        <li>
          <i class="icomoon-whatsapp"></i> <a href="tel:+5511900000000">+55 11 90000-0000</a>
        </li>
      </ul>
    </footer>

  </body>
</html>
